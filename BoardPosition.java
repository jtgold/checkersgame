package cpsc2150.hw5;

/*
    This class just holds three variables for the game. The row, col, and what letter is in that row and columns
 */
/**
 * @invariant row > 0 && row < 8 and col > 0 && col <8
 * player == 'X' || player == 'O'
 */
public class BoardPosition {
    private int row;
    private int col;
    private char player;

    /**
     *
     * @param row1 is the rows in the array
     * @param col1 is the columns in the array
     * @param players is what player is currently playing
     *
     * @requires
     * row1 > 0 && row < 8
     * col1 > 0 && col < 8
     * players == 'X' || players == 'O'
     *
     * @ensures
     * row = row1
     * col = col1
     * player = players
     */
    BoardPosition(int row1, int col1, char players){
        row = row1; col = col1; player = players;
    }

    /**
     *
     * @return row
     * @ensures getRow() = row
     */
    public int getRow(){
        return row;
    }

    /**
     *
     * @return col
     * @ensures getCol() = col
     */
    public int getCol(){
        return col;
    }

    /**
     *
     * @return player
     * @ensures getPlayer() = player
     */
    public char getPlayer(){
        return player;
    }


    //@Override
    public boolean equals(BoardPosition test){
        if(test.getPlayer() == this.getPlayer() && test.getCol() == this.getCol() && test.getRow() == this.getRow())
            return true;
        else
            return false;
    }
}