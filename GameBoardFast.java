package cpsc2150.hw5;
/*
    This is where the game mechanics are basically at. This class determines if it is legal to place something in
    the array, places it in the array, and checks for a win or a draw
 */
/**
 * @Invariants BoardPosition != null
 * row <= MAX_SIZE && rows > 0 && cols <= MAX_SIZE && cols > 0
 */
public class GameBoardFast implements IGameBoard {
    private char[][] board;
    int rows;
    int cols;
    int numToWin;

    /**
     * @requires r <= MAX_SIZE, c <= MAX_SIZE, r > 0, c > 0
     * @ensure r = rows, c = cols, ntw = numToWin
     */
    GameBoardFast(int r, int c, int ntw){
        numToWin = ntw;
        rows = r;
        cols = c;
        board = new char[rows + 1][cols + 1];
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                board[i][j]= ' ';
            }
        }
    }


    public boolean checkSpace(BoardPosition pos){
        if(pos.getCol() > cols - 1 || pos.getCol() < 0 || pos.getRow() > rows - 1 || pos.getRow() < 0)
            return false;
        if(board[pos.getRow()][pos.getCol()] != ' ')
            return false;
        return true;
    }

    public void placeMarker(BoardPosition marker){
        board[marker.getRow()][marker.getCol()] = marker.getPlayer();
    }


    public boolean checkForDraw(){
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(board[i][j] == ' ')
                    return false;
            }
        }
        return true;
    }


    public boolean checkForWinner(BoardPosition lastPos){
        boolean win;
        win = checkHorizontalWin(lastPos);
        if(win)
            return win;
        win = checkVerticalWin(lastPos);
        if(win)
            return win;
        win = checkDiagonalWin(lastPos);
        if(win)
            return win;

        return win;
    }

    /**
     *
     * @param lastPos uses the data to check if there is a horizontal win
     * @return true if there is a win and false otherwise
     * @requires lastPos != null
     * @ensure if there are horizontal placed chars = numToWin the game will be over
     */
    private boolean checkHorizontalWin(BoardPosition lastPos){
        int counter = 0;
        boolean win = false;
        for(int i = 0; i < cols; i++){
            if(board[lastPos.getRow()][i] == lastPos.getPlayer())
                counter++;
            if(board[lastPos.getRow()][i] != lastPos.getPlayer())
                counter = 0;
            if(counter == numToWin)
                win = true;
        }
        return win;
    }

    /**
     *
     * @param lastPos uses data to check for a vertical win
     * @return true if there is a win and false otherwise
     * @requires lastPos != null
     *@ensure if there are vertical placed chars = numToWin the game will be over
     */
    private boolean checkVerticalWin(BoardPosition lastPos){
        int counter = 0;
        boolean win = false;
        for(int i = 0; i < rows; i++){
            if(board[i][lastPos.getCol()] == lastPos.getPlayer())
                counter++;
            if(board[i][lastPos.getCol()] != lastPos.getPlayer())
                counter = 0;
            if(counter == numToWin)
                win = true;
        }
        return win;

    }

    /**
     * @param lastPos uses data to check for a diagonal win
     * @return true if there is a win and false otherwise
     * @requires lastPos != null
     * @ensure if there are diagonal placed chars = numToWin the game will be over
     */
    /*
    This function first checks the diagonal from bottom left to top right using two while loops
    It then checks for a diagonal win in the opposite direction using the same method
     */
    private boolean checkDiagonalWin(BoardPosition lastPos){
        int counter = 0; int newRow = lastPos.getRow(); int newCol = lastPos.getCol();
        boolean same = true;

        while(newRow >= 0 && newCol <= cols && same){
            if(board[newRow][newCol] == lastPos.getPlayer())
                counter++;
            if(board[newRow][newCol] != lastPos.getPlayer())
                same = false;
            newRow--; newCol++;

        }
        newRow = lastPos.getRow() + 1; newCol = lastPos.getCol() - 1; same = true;
        while(newRow <= rows && newCol >= 0 && same){
            if(board[newRow][newCol] == lastPos.getPlayer())
                counter++;
            if(board[newRow][newCol] != lastPos.getPlayer())
                same = false;
            newRow++; newCol--;
        }
        if(counter == numToWin)
            return true;
        counter = 0;
        newRow = lastPos.getRow(); newCol = lastPos.getCol(); same = true;
        while(newRow >= 0 && newCol >= 0 && same){
            if(board[newRow][newCol] == lastPos.getPlayer())
                counter++;
            if(board[newRow][newCol] != lastPos.getPlayer())
                same = false;
            newRow--; newCol--;
        }

        newRow = lastPos.getRow() + 1; newCol = lastPos.getCol() + 1; same = true;
        while(newRow <= rows && newCol <= cols && same){
            if(board[newRow][newCol] == lastPos.getPlayer())
                counter++;
            if(board[newRow][newCol] != lastPos.getPlayer())
                same = false;
            newRow++; newCol++;
        }
        if(counter == numToWin)
            return true;
        counter = 0;
        return false;
    }

    /**
     *
     * @return a string with the whole gameboard
     * @ensures the whole gameboard will be printed
     */
    @Override
    public String toString() {
        String str = "";

        if(cols <= 10)
            str += "  ";
        if(cols > 10 && rows <= 10)
            str += "   ";
        if(cols > 10 && rows > 10)
            str += "    ";
        if(cols <= 10 && rows > 10)
            str += " ";
        for(int i = 0; i < cols; i++) {
            if (cols <= 10)
                str += i + "|";
            if (cols > 10) {
                if (i < 10)
                    if(i == 9)
                        str+= i + "|";
                    else
                        str += i + "| ";
                if(i >= 10)
                    str += i + "|";
            }
        }

        for(int i = 0; i < rows; i++){
            if(rows <= 10 && cols <= 10)
                str += "\n" + i + "|";
            if(rows <= 10 && cols > 10)
                str+= "\n" + i + "| ";
            if(rows > 10 && i < 10)
                str += "\n " + i + "|";
            if(rows > 10 && i >= 10)
                str += "\n" + i + "|";

            for(int j = 0; j < cols; j++) {
                if (cols <= 10)
                    str += board[i][j] + "|";
                if (cols > 10) {
                    if (j < 10)
                        str += board[i][j] + "| ";

                    if(j >= 10 && board[i][j] == ' ')
                        str += board[i][j] + "| ";
                    if(j >= 10 && board[i][j] != ' ')
                        str += board[i][j] + "|";
                }

            }

        }


        return str;
    }

}